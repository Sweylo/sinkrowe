package sinkrowe;

import com.jcraft.jsch.*;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import java.util.ArrayList;
import java.util.Vector;

/**
 * @author sweylo
 */

public class Sftp {
    
    private String host;
    private String user;
    private String pass;
    private String path;
    private int port;
    private ChannelSftp ch;
    private JSch jsch;
    private Session sess;
    private UserInfo userInfo;
    
    /**
     * Creates and handles an SFTP connection to a remote server using JSch
     * @param host hostname of server
     * @param user user to connect with
     * @param pass password of the user (plaintext)
     * @param port port to connect with
     * @param path initial path
     */
    
    public Sftp (String host, String user, String pass, int port, String path) {
        
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.port = port;
        this.path = path;
        jsch = new JSch();
        userInfo = new MyUserInfo(this.pass);
        
        try {
            
            sess = jsch.getSession(this.user, this.host, this.port);
            sess.setUserInfo(userInfo);
            sess.connect();
            Channel channel = sess.openChannel("sftp");
            channel.connect();
            ch = (ChannelSftp)channel;
            
        } catch (JSchException ex) {
            System.err.println(ex.toString());
            System.exit(2);
        }
        
    }
    
    /**
     * Changes remote directory
     * @param path changes directory to the path passed
     */
    
    public void cd(String path) {
        
        this.path = path;
        
        try {
            ch.cd(this.path);
        } catch (SftpException ex) {
            System.err.println(ex.toString());
        }
        
    }
    
    /**
     * Returns an arraylist containing the current directory listing
     * @return arraylist containing the sorted directory listing
     */
    
    public ArrayList<RemoteFile> ls() {
        
        Vector list = null;
        ArrayList<RemoteFile> out = new ArrayList<>();
        
        try {
            list = ch.ls(path);
        } catch (SftpException ex) {
            System.err.println(ex.toString());
        }
        
        if (list != null) {
            for (Object listing : list) {
                String item = ((LsEntry) listing).getFilename();
                if (
                    listing instanceof LsEntry 
                    && 
                    !item.equals(".")
                    && 
                    !item.equals("..")
                ) {
                    out.add(new RemoteFile(path + item, this));
                }
            }
        }
        
        //Collections.sort(out);
        
        return out;
        
    }
    
    /**
     * Returns an arraylist containing the directory listing
     * @param path path of directory to return listing of
     * @return arraylist containing the sorted directory listing
     */
    
    public ArrayList<RemoteFile> ls(String path) {
        
        Vector list = null;
        ArrayList<RemoteFile> out = new ArrayList<>();
        
        try {
            //System.out.println((ch == null) ? "true" : "false");
            list = ch.ls(path);
        } catch (SftpException ex) {
            System.err.println(ex.toString());
        }
        
        if (list != null) {
            for (Object listing : list) {
                String item = ((LsEntry) listing).getFilename();
                if (
                    listing instanceof LsEntry 
                    && 
                    !item.equals(".")
                    && 
                    !item.equals("..")
                ) {
                    out.add(new RemoteFile(path + item, this));
                }
            }
        }
        
        //Collections.sort(out);
        
        return out;
        
    }
    
    /**
     * Downloads from remote to local
     * @param remoteFilePath path to the remote file
     * @param localFilePath path to the local file
     */
    
    public void get(
        String remoteFilePath, 
        String localFilePath, 
        SftpProgressMonitor spm,
        int mode
    ) {
        
        //int mode = ChannelSftp.OVERWRITE;
        
        try {
            ch.get(remoteFilePath, localFilePath, spm, mode);
        } catch (SftpException ex) {
            System.err.println(ex.toString());
        }
        
    }
    
    /**
     * Uploads from local to remote
     * @param remoteFilePath path to the remote file
     * @param localFilePath path to the local file
     */
    
    public void put(
        String remoteFilePath, 
        String localFilePath, 
        SftpProgressMonitor spm,
        int mode
    ) {
        
        //int mode = ChannelSftp.OVERWRITE;
        
        try {
            ch.put(localFilePath, remoteFilePath, spm, mode);
        } catch (SftpException ex) {
            System.err.println(ex.toString());
        }
        
    }
    
    public SftpATTRS stat(String filePath) {
        
        SftpATTRS attrs = null;
        
        try {
            attrs = ch.stat(filePath);
        } catch (SftpException e) {
            System.err.println(e.toString());
        }
        
        return attrs;
        
    }
    
    /**
     * Returns the current path
     * @return current path
     */
    
    public String getPath() {
        return path;
    }
    
}
