package sinkrowe;

import com.jcraft.jsch.UserInfo;

/**
 * @author sweylo
 */

public class MyUserInfo implements UserInfo {
        
    String passwd;

    public MyUserInfo(String pass) {
        passwd = pass;
    }

    @Override
    public String getPassphrase() {
        return null;
    }

    @Override
    public String getPassword() {
        return passwd;
    }

    @Override
    public boolean promptPassword(String string) {
        return true;        
    }

    @Override
    public boolean promptPassphrase(String string) {
        return true;        
    }

    @Override
    public boolean promptYesNo(String string) {
        return true;
    }

    @Override
    public void showMessage(String string) {

    }

}
