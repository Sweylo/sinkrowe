package sinkrowe;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpProgressMonitor;
import java.io.File;

/**
 * @author sweylo
 */

public class SyncFile implements SftpProgressMonitor {
    
    private final RemoteFile remoteFile;
    private final LocalFile localFile;
    private final String relPath;
    private final Sftp sftp;
    public boolean isWorking;
    
    public SyncFile(RemoteFile rf, LocalFile lf, String rp, Sftp s) {
        remoteFile = rf;
        localFile = lf;
        relPath = rp;
        sftp = s;
        isWorking = false;
    }
    
    public void sync() {
        
        if (localIsNewer()) {
            
            sftp.put(
                remoteFile.getPath(), 
                localFile.getPath(), 
                this,
                ChannelSftp.OVERWRITE
            );
            
        } else if (remoteIsNewer()) {
            
            sftp.get(
                remoteFile.getPath(), 
                localFile.getPath(), 
                this,
                ChannelSftp.OVERWRITE
            );
            
        }
        
    }
    
    public boolean bothAreSynced() {
        return 
            (
                remoteFile.getAttrs().getMTime() 
                ==
                localFile.lastModified() / 1000
            )
            &&
            (
                remoteFile.getAttrs().getSize()
                ==
                localFile.length()
            )
        ;
    }
    
    public boolean localIsNewer() {
        return 
            remoteFile.getAttrs().getMTime() 
            <
            localFile.lastModified() / 1000
        ;
    }
    
    public boolean remoteIsNewer() {
        return 
            remoteFile.getAttrs().getMTime() 
            >
            localFile.lastModified() / 1000
        ;
    }
    
    @Override
    public void init(int op, String src, String dst, long max) {
        isWorking = true;
    }

    @Override
    public boolean count(long count) {
        //System.out.println(count);
        return false;
    }

    @Override
    public void end() {
        remoteFile.refresh(sftp);
        localFile.setLastModified(
            ((long) remoteFile.getAttrs().getMTime()) * 1000
        );
        isWorking = false;
    }

    public RemoteFile getRemoteFile() {
        return remoteFile;
    }

    public File getLocalFile() {
        return localFile;
    }
    
    public String getRelPath() {
        return relPath;
    }
    
}
