package sinkrowe;

import com.jcraft.jsch.SftpATTRS;

/**
 * @author sweylo
 */

public class RemoteFile {
    
    private String path;
    private SftpATTRS attrs;
    public boolean processed;
    public boolean isCommon;
    
    public RemoteFile(String path, Sftp sftp) {
        this.path = path;
        attrs = sftp.stat(path);
        processed = false;
        isCommon = false;
    }
    
    public RemoteFile(String path, SftpATTRS attrs) {
        this.path = path;
        this.attrs = attrs;
    }
    
    public String getPath() {
        return path;
    }
    
    public SftpATTRS getAttrs() {
        return attrs;
    }
    
    public void refresh(Sftp sftp) {
        attrs = sftp.stat(path);
    }
    
    public boolean isDir() {
        return attrs.isDir();
    }
    
    public long getSize() {
        return attrs.getSize();
    }
    
}
