package sinkrowe;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author sweylo
 */

public class Sinkrowe {
    
    private static ArrayList<LocalFile> localList;
    private static ArrayList<RemoteFile> remoteList;
    private static ArrayList<SyncFile> commonList;
    private static ArrayList<LocalFile> localOnlyList;
    private static ArrayList<RemoteFile> remoteOnlyList;
    private static Sftp sftp;
    private static String remoteDir;
    private static String localDir;
    private static LocalFile local;
    private static long scanTime;
    private static long analyzeTime;
    
    public static void init() {
        remoteDir = "/home/sinkrowe/";
        localDir = "/home/sweylo/sinkrowe/";
        local = new LocalFile(localDir);
        localList = new ArrayList<>();
        remoteList = new ArrayList<>();
        commonList = new ArrayList<>();
        localOnlyList = new ArrayList<>();
        remoteOnlyList = new ArrayList<>();
        sftp = new Sftp(
            "localhost",
            "sinkrowe",
            "sinkrowe",
            22,
            remoteDir
        );
    }

    public static void main(String[] args) {
        
        init();
        scanFiles();
        analyzeFiles();
        
        dispTime("Scan", scanTime);
        dispTime("Analyze", analyzeTime);
        dispTime("Total", scanTime + analyzeTime);
        
        System.out.println("\nCommon:");
        for (SyncFile sf : commonList) {
            if (
                !sf.isWorking
                &&
                sf.getLocalFile().canRead() 
                && 
                sf.getLocalFile().canWrite()
            ) {
                System.out.println(
                    "\t" + 
                    sf.getRelPath() + 
                    " | Synced: " + 
                    sf.bothAreSynced() + 
                    " | Local: " + 
                    sf.getLocalFile().lastModified() / 1000 + 
                    " | Remote: " + 
                    sf.getRemoteFile().getAttrs().getMTime()
                );
                //sf.sync();
            }
        }
        
        System.out.println("Remote only:");
        for (RemoteFile rf : remoteOnlyList) {
            System.out.println("\t" + rf.getPath());
        }
        System.out.println("Local only:");
        for (LocalFile lf : localOnlyList) {
            System.out.println("\t" + lf.getAbsolutePath());
        }
        
        System.exit(0);
        
    }
    
    public static String getRelPath(String root, String abs) {
        return abs.replaceFirst(root, "");
    }
    
    public static void scanFiles() {
        long initTime = System.nanoTime();
        scanLocalFiles(local);
        scanRemoteFiles(remoteDir);
        scanTime = System.nanoTime() - initTime;
    }
    
    public static void analyzeFiles() {
        
        long initTime = System.nanoTime();
        
        for (RemoteFile rf : remoteList) { // check for common first
            
            for (LocalFile lf : localList) {
                
                if (
                    getRelPath(remoteDir, rf.getPath()).equals(
                        getRelPath(localDir, lf.getAbsolutePath())
                    )
                ) {
                    commonList.add(
                        new SyncFile(
                            rf, 
                            lf,
                            getRelPath(remoteDir, rf.getPath()), 
                            sftp
                        )
                    );
                    rf.processed = true;
                    lf.processed = true;
                    rf.isCommon = true;
                    lf.isCommon = true;
                }
                
            }
            
        }
        
        for (RemoteFile rf : remoteList) { // put remainders into "only" lists
            
            for (LocalFile lf : localList) {
                if (!rf.processed && !rf.isCommon) {
                    remoteOnlyList.add(rf);
                    rf.processed = true;
                }
                if (!lf.processed && !lf.isCommon) {
                    localOnlyList.add(lf);
                    lf.processed = true;
                }      
            }
            
        }
        
        analyzeTime = System.nanoTime() - initTime;
                
    }
    
    public static void scanLocalFiles(File dir) {
        
        try {
            
            ArrayList<File> files = new ArrayList<>(
                Arrays.asList(dir.listFiles())
            );
            
            for (File f : files) {
                //System.out.println("local:" + f.getPath());
                if (f.isDirectory()) {
                    scanLocalFiles(f);
                } else {
                    localList.add(new LocalFile(f));
                }
            }
            
        } catch (java.lang.NullPointerException e) {
            System.err.println("Unable to open: " + dir);
            System.exit(1);
        }
             
        
              
    }
    
    public static void scanRemoteFiles(String dir) {
        
        ArrayList<RemoteFile> files = sftp.ls(dir);
        
        for (RemoteFile rf : files) {
            //System.out.println("remote:" + rf.getPath());
            if (rf.isDir()) {
                scanRemoteFiles(rf.getPath() + "/");
            } else {
                remoteList.add(rf);
            }
        }
        
    }
    
    private static void dispTime(String name, long time) {
        System.out.printf(
            "%7s: %10.3f ms\n", 
            name,
            (double)time / 1000000.00
        );
    }
    
}
