package sinkrowe;

import java.io.File;

/**
 * @author sweylo
 */

public class LocalFile extends File {
    
    public boolean processed;
    public boolean isCommon;
    
    public LocalFile(String path) {
        super(path);
        processed = false;
        isCommon = false;
    }
    
    public LocalFile(File file) {
        super(file.getAbsolutePath());
    }
    
}
